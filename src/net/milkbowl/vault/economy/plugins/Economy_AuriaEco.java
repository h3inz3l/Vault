package net.milkbowl.vault.economy.plugins;

import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;

import me.auria.plugins.auriacore.core.eco.AuriaEco;
import net.milkbowl.vault.economy.AbstractEconomy;
import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;

public class Economy_AuriaEco extends AbstractEconomy {
  private static final Logger log = Logger.getLogger("Minecraft");

  private String name = "AuriaEco";
  private Plugin plugin;

  public Economy_AuriaEco(Plugin plugin) {
    this.plugin = plugin;
    Bukkit.getServer().getPluginManager().registerEvents(new Economy_AuriaEco.EconomyServerListener(this), plugin);

    // Load Plugin in case it was loaded before
    Plugin ec = plugin.getServer().getPluginManager().getPlugin("AuriaCore");
    if (ec != null && ec.isEnabled()) {
      log.info(String.format("[%s][Economy] %s hooked.", plugin.getDescription().getName(), name));
    }
  }

  public class EconomyServerListener implements Listener {
    Economy_AuriaEco economy = null;

    public EconomyServerListener(Economy_AuriaEco economy) {
      this.economy = economy;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPluginEnable(PluginEnableEvent event) {
      Plugin ec = event.getPlugin();
      if (ec.getName().equals("AuriaCore")) {
        log.info(String.format("[%s][Economy] %s hooked.", plugin.getDescription().getName(), economy.name));
      }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPluginDisable(PluginDisableEvent event) {
      if (event.getPlugin().getName().equals("AuriaCore")) {
        log.info(String.format("[%s][Economy] %s unhooked.", plugin.getDescription().getName(), economy.name));
      }
    }
  }

  private Player p(String name) {
    return Bukkit.getPlayer(name);
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String format(double amount) {
    return AuriaEco.format(amount);
  }

  @Override
  public String currencyNameSingular() {
    return AuriaEco.getCurrencyNameSingular();
  }

  @Override
  public String currencyNamePlural() {
    return AuriaEco.getCurrencyNamePlural();
  }

  @Override
  public double getBalance(String playerName) {
    return AuriaEco.getBalance(p(playerName));
  }

  @Override
  public EconomyResponse withdrawPlayer(String playerName, double amount) {
    if (AuriaEco.withdraw(p(playerName), amount)) {
      return new EconomyResponse(amount, getBalance(playerName), ResponseType.SUCCESS, null);
    }
    return new EconomyResponse(0, 0, ResponseType.FAILURE, "Cannot withdraw negative funds");
  }

  @Override
  public EconomyResponse depositPlayer(String playerName, double amount) {
    if (AuriaEco.deposit(p(playerName), amount)) {
      return new EconomyResponse(amount, getBalance(playerName), ResponseType.SUCCESS, null);
    }
    return new EconomyResponse(0, 0, ResponseType.FAILURE, "Cannot desposit negative funds");
  }

  @Override
  public boolean has(String playerName, double amount) {
    return getBalance(playerName) >= amount;
  }

  @Override
  public EconomyResponse createBank(String name, String player) {
    throw new UnsupportedOperationException("AuriaEco does not support Banks");
  }

  @Override
  public EconomyResponse deleteBank(String name) {
    throw new UnsupportedOperationException("AuriaEco does not support Banks");
  }

  @Override
  public EconomyResponse bankHas(String name, double amount) {
    throw new UnsupportedOperationException("AuriaEco does not support Banks");
  }

  @Override
  public EconomyResponse bankWithdraw(String name, double amount) {
    throw new UnsupportedOperationException("AuriaEco does not support Banks");
  }

  @Override
  public EconomyResponse bankDeposit(String name, double amount) {
    throw new UnsupportedOperationException("AuriaEco does not support Banks");
  }

  @Override
  public EconomyResponse isBankOwner(String name, String playerName) {
    throw new UnsupportedOperationException("AuriaEco does not support Banks");
  }

  @Override
  public EconomyResponse isBankMember(String name, String playerName) {
    throw new UnsupportedOperationException("AuriaEco does not support Banks");
  }

  @Override
  public EconomyResponse bankBalance(String name) {
    throw new UnsupportedOperationException("AuriaEco does not support Banks");
  }

  @Override
  public List<String> getBanks() {
    throw new UnsupportedOperationException("AuriaEco does not support Banks");
  }

  @Override
  public boolean hasBankSupport() {
    return false;
  }

  @Override
  public boolean hasAccount(String playerName) {
    return true;
  }

  @Override
  public boolean createPlayerAccount(String playerName) {
    return true;
  }

  @Override
  public int fractionalDigits() {
    return -1;
  }

  @Override
  public boolean hasAccount(String playerName, String worldName) {
    return hasAccount(playerName);
  }

  @Override
  public double getBalance(String playerName, String world) {
    return getBalance(playerName);
  }

  @Override
  public boolean has(String playerName, String worldName, double amount) {
    return has(playerName, amount);
  }

  @Override
  public EconomyResponse withdrawPlayer(String playerName, String worldName, double amount) {
    return withdrawPlayer(playerName, amount);
  }

  @Override
  public EconomyResponse depositPlayer(String playerName, String worldName, double amount) {
    return depositPlayer(playerName, amount);
  }

  @Override
  public boolean createPlayerAccount(String playerName, String worldName) {
    return createPlayerAccount(playerName);
  }

}
